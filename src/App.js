import React, { useEffect, useState } from "react";
import {
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  CssBaseline,
  Grid,
  Typography,
  Container,
  Link,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";

// Components
import MainFeaturedPost from "./Components/MainFeaturedPost";
import Header from "./Components/Header";

const mainFeaturedPost = {
  title: "",
  description: "",
  image: "https://source.unsplash.com/random",
  imgText: "",
  linkText: "",
};

const sections = [
  { title: "N THEATERS", url: "#" },
  { title: "COMING SOON", url: "#" },
  { title: "CHART", url: "#" },
  { title: "TV SERIES", url: "#" },
  { title: "TRAILERS", url: "#" },
  { title: "MORE", url: "#" },
];

function Copyright() {
  return (
    <Typography variant='body2' color='textSecondary' align='center'>
      {"Copyright © "}
      <Link color='inherit' href='https://material-ui.com/'>
        Your Website
      </Link>{" "}
      {new Date().getFullYear()}
      {"."}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(0, 0, 0),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(6),
  },
}));

const cards = [1, 2, 3, 4, 5, 6, 7, 8, 9];

export default function App() {
  const classes = useStyles();
  const [state, setState] = useState({
    dataMovies: [],
  });

  const { dataMovies } = state;

  useEffect(() => {
    fetchDataMovies();
  }, []);

  const fetchDataMovies = async () => {
    let headers = {
      Authorization:
        "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIwNGUxYzQyODNhMzk3M2M1MTc1ZGZiMzE5MTlhZTNiMiIsInN1YiI6IjVmZDY0NDdhMDcyOTFjMDA0MDc0Mzg0ZCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.sbmfsP7Vdajb8yUWWRmWspTX3MQbKFcHas29g37arn8",
      "Content-Type": "application/json;charset=utf-8",
    };

    fetch("https://api.themoviedb.org/3/trending/movie/week", {
      headers,
    })
      .then((response) => response.json())
      .then((responseData) => {
        console.log("responseData", responseData);
        setState({
          ...state,
          dataMovies: responseData.results,
        });
      })
      .catch((error) => console.log("error responseData", error.response));
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Header title='Layar Kaca 21' sections={sections} />
      <main>
        {/* Hero unit */}
        <div className={classes.heroContent}>
          <MainFeaturedPost post={dataMovies} />
        </div>
        <Container className={classes.cardGrid} maxWidth='md'>
          {/* End hero unit */}
          <Grid container justify='center' spacing={4}>
            {dataMovies.length > 0 ? (
              dataMovies.map((key, index) => (
                <Grid item key={index} xs={12} sm={6} md={4}>
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image={`https://image.tmdb.org/t/p/original${key.backdrop_path}`}
                      title={key.title}
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant='h5' component='h2'>
                        {key.title}
                      </Typography>
                      <Typography>{key.overview}</Typography>
                    </CardContent>
                    {/* <CardActions>
                      <Button size='small' color='primary'>
                        View
                      </Button>
                      <Button size='small' color='primary'>
                        Edit
                      </Button>
                    </CardActions> */}
                  </Card>
                </Grid>
              ))
            ) : (
              <Grid item>
                <p>Loading data ...</p>
              </Grid>
            )}
          </Grid>
        </Container>
        <Container maxWidth='sm'>
          <Typography
            component='h1'
            variant='h2'
            align='center'
            color='textPrimary'
            gutterBottom
          >
            About Author
          </Typography>
          <Typography
            variant='h5'
            align='center'
            color='textSecondary'
            paragraph
          >
            Hello my name is{" "}
            <span>
              <a
                target='_blank'
                style={{ color: "grey" }}
                href='linkedin.com/in/wahyu-fatur-rizky/'
              >
                Wahyu Fatur Rizki
              </a>
              , you can call me Wahyu. I create this web Layar Kaca 21 just for
              FUN because I'm Tech Enthusiast
            </span>
          </Typography>
          <div className={classes.heroButtons}>
            <Grid container spacing={2} justify='center'>
              <Grid item>
                <Button
                  onClick={() =>
                    window.open("https://gitlab.com/wahyufaturrizky", "_blank")
                  }
                  variant='contained'
                  color='primary'
                >
                  My Gitlab Projects
                </Button>
              </Grid>
              <Grid item>
                <Button
                  onClick={() =>
                    window.open(
                      "https://www.linkedin.com/in/wahyu-fatur-rizky/",
                      "_blank"
                    )
                  }
                  variant='outlined'
                  color='primary'
                >
                  My LinkedIn
                </Button>
              </Grid>
            </Grid>
          </div>
        </Container>
      </main>
      {/* Footer */}
      <footer className={classes.footer}>
        <Typography variant='h6' align='center' gutterBottom>
          Footer
        </Typography>
        <Typography
          variant='subtitle1'
          align='center'
          color='textSecondary'
          component='p'
        >
          Something here to give the footer a purpose!
        </Typography>
        <Copyright />
      </footer>
      {/* End footer */}
    </React.Fragment>
  );
}
